<!DOCTYPE html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ21QB3167"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ21QB3167');
</script>
<meta charset="utf-8">
<title>HernáZóna.eu | VIP</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">



<link rel="shortcut icon" href="images/hzlogodogo2.png" type="image/x-icon">
<link rel="icon" href="images/hzlogodogo2.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-two">
        <div class="header-container">

            <!-- Header Top
            <div class="header-top">
            	<div class="auto-container">
                    <div class="inner clearfix">
                        <div class="top-left">
                            <div class="top-text">Vitajte na portáli HernáZóna.eu</div>
                        </div>
        
                        <div class="top-right">
                            <ul class="info clearfix">
                                <li><a href="#">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Header Upper -->
            <div class="header-upper">
            	<div class="auto-container">
                    <div class="inner-container clearfix">
                        <!--Logo-->
                        <div class="logo-box">
                            <div class="logo"><a href="index.php" title=""><img src="images/hzlogodogo.png" alt="" title="">HernáZóna.eu</a></div>
                        </div>
    
                        <!--Nav Box-->
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span></div>
    
                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="index.php">Domov</a></li>
                                        <li><a href="about.php">O nás</a></li>
                                        <li><a href="servers.php">Servery</a></li>
                                        <!-- <li><a href="gallery.php">Galéria</a></li> -->
                                        <li><a href="team.php">Team</a></li>
                                        <li class="current"><a href="vip.php">VIP</a></li>
                                        <li><a href="faq.php">Faq</a></li>
                                        <li class="dropdown"><a href="rules.php">Podmienky</a>
                                            <ul>
                                                <li><a href="rules.php">Pravidlá</a></li>
                                                <li><a href="gdpr.php">GDPR</a></li>
                                                <li><a href="cookies.php">Cookies</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.php">Kontakt</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            <ul class="social-links clearfix">
                                <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--End Header Upper-->
        </div><!--End Header Container-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/hzlogodogo2.png" alt="" title="">HernáZóna.eu</a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix top5">
                        <!--Javascript Load-->
                    </nav><!-- Main Menu End-->

                    <ul class="social-links clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                <div class="menu-outer"><!--Javascript Load--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->

    <!--Page Title-->
    <section class="page-banner" style="background-image:url(images/main-slider/huh.jpg);">
        <div class="top-pattern-layer"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>VIP</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Banner-->

    <section class="servers-section">
        <div class="top-pattern-layer"></div>

        <div class="auto-container">
            <div class="contr">
            <div class="pricing-table table1">
                <div class="pricing-header">
                    <div class="price">4,50 <span>€</span>/99 <span>Kč</span></div>
                    <div class="vip-title">VIP</div>
                </div>
                <ul class="pricing-list">
                    <li>Väčšia rezidencia</li>
                    <div class="vip-border"></div>
                    <li>Viac domovov</li>
                    <div class="vip-border"></div>
                    <li>Extra príkazy</li>
                    <div class="vip-border"></div>
                    <li>Doba 30 dní</li>
                    <div class="vip-border"></div>
                    <li>Prefix <span style="color: #fff;">[</span><span style="color: #00aa00;">VIP</span><span style="color: #fff;">]</span></li>
                    <div class="vip-border"></div>
                </ul>
                <a href="#VIP">Viac</a>
            </div>
            <div class="pricing-table table2">
                <div class="pricing-header">
                    <div class="price">8,00 <span>€</span>/249 <span>Kč</span></div>
                    <div class="vip-title">SuperVIP</div>
                </div>
                <ul class="pricing-list">
                    <li>Väčšia rezidencia</li>
                    <div class="vip-border"></div>
                    <li>Viac domovov</li>
                    <div class="vip-border"></div>
                    <li>Extra príkazy</li>
                    <div class="vip-border"></div>
                    <li>Doba 30 dní</li>
                    <div class="vip-border"></div>
                    <li>Prefix <span style="color: #fff;">[</span><span style="color: #ff55ff;">SuperVIP</span><span style="color: #fff;">]</span></li>
                    <div class="vip-border"></div>
                </ul>
                <a href="#SVIP">Viac</a>
            </div>
            <div class="pricing-table table3">
                <div class="pricing-header">
                    <div class="price">15 <span>€</span></div>
                    <div class="vip-title">Sponsor</div>
                </div>
                <ul class="pricing-list">
                    <li>Väčšia rezidencia</li>
                    <div class="vip-border"></div>
                    <li>Viac domovov</li>
                    <div class="vip-border"></div>
                    <li>Extra príkazy</li>
                    <div class="vip-border"></div>
                    <li>Doba 30 dní</li>
                    <div class="vip-border"></div>
                    <li>Prefix <span style="color: #fff;">[</span><span style="color: #ffaa00;">Sponsor</span><span style="color: #fff;">]</span></li>
                    <div class="vip-border"></div>
                </ul>
                <a href="#sposor">Viac</a>
            </div>
        </div>
        </div>

        <div class="medzera-20"></div>

        <div class="auto-container">
        <div class="sec-title centered"><h2>Tabuľka výhod</h2><span class="bottom-curve"></span></div>
        <div class="progress-table-wrap">
        <table>
            <tr>
                <th></th>
                <th>Hráč</th>
                <th>VIP</th>
                <th>SuperVIP</th>
                <th>Sponsor</th>
            </tr>
            <div clas="vip-border-vyhody"></div>
            <tr>
                <th>Veľkosť rezidencie</th>
                <td>100x100</td>
                <td>125x125</td>
                <td>175x175</td>
                <td>200x200</td>
            </tr>
            <tr>
                <th>Počet rezidencií</th>
                <td>3</td>
                <td>4</td>
                <td>6</td>
                <td>8</td>
            </tr>
            <tr>
                <th>Počet domovov</th>
                <td>3</td>
                <td>5</td>
                <td>9</td>
                <td>16</td>
            </tr>
            <tr>
                <th>Kit</th>
                <td>/kit tools</td>
                <td>/kit vip</td>
                <td>/kit svip</td>
                <td>/kit sponsor</td>
            </tr>
            <tr>
                <th>Kit použitie</th>
                <td class="red">1</td>
                <td>&infin; (1x za 1hod)</td>
                <td>&infin; (1x za 1hod)</td>
                <td>&infin; (1x za 1hod)</td>
            <tr>
                <th>Farebný chat</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>Farebný nick</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>Strata vecí a xp pri smrti</th>
                <td class="red">&#x2714;&#xFE0E;</td>
                <td class="red">&#x2714;&#xFE0E;</td>
                <td>&#x2716;&#xFE0E;</td>
                <td>&#x2716;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/fly</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/ext</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/nick</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/hat</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/feed</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/top</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/craft</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/jump</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/heal</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/enderchest</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/enchant</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/repair</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/firework</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/day</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/night</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
            <tr>
                <th>/setwarp</th>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td class="red">&#x2716;&#xFE0E;</td>
                <td>&#x2714;&#xFE0E;</td>
            </tr>
        </table>
        </div>
        </div>

        <div class="medzera-50"></div>
        <div class="auto-container">
            <h2 class="red">Upozornenie</h2>
            <div class="medzera-20"></div>
            <blockquote class="generic-blockquote">
                Platené varianty VIP sú predovšetkým pre podporu portálu a nie pre upevnenie vášho postavenia na portáli. Odmeny sú takým bonusom, ktoré sa môžu kedykoľvek rozšíriť, zmeniť alebo úplne zrušiť. Prípadné reklámácie u nás sú možné len po dohode s majiteľom portálu a výška vrátenia čiastky je čiastka, ktorú sme obdržali (teda cena produktu po odčítaní poplatkov a podielov tretích strán). Pri zle zaslanej platbe kontaktujte prevádzkovateľa platobnej služby. Pri platbe pomocou SMS si prosím skontrolujte, či máte aktivované prémium SMS (pokiaľ služba nie je aktivovaná, prípadná platba nebude prevedená). Pre riešenie problémov alebo doplnenie informácií kontaktujte členov <b>Admin tímu</b>, tiež nás môžete kontaktovať prostredníctvom emailu <a href="mailto:info@hernazona.eu">info@hernazona.eu</a> alebo na našom <a href="https://discord.gg/vnPFP42DUA" target="_blank">Discorde</a>.
            </blockquote>

            <div class="medzera-50" id="VIP"></div>

            <h2 style="color: #006400">VIP</h2>
            <div class="medzera-20"></div>
            <p>VIP je prvým stupňom odmeny za podporu portálu. Poskytuje zmenu parametrov nastavenia rezidencií ako napríklad navýšenie maximálneho počtu rezidencií na 4 s maximálnou veľkosťou 125x125 blockov. Poskytuje tiež celkovo si vvytvoriť až 5 domovov a môže použiť príkaz /fly.</p>
            
            <div class="medzera-20"></div>

            <div class="flexx">
                <div class="col-md-6" style="margin-right: 190px;">

                    <h3 class="mb-20">SMS Platba</h3>
                    <p>SMS formát pre SR: <b>SERVER PRO 118119 4.5 VIP {meno na serveri}</b></p>
                    <p>Telefónne číslo pre SR: <b>8866</b></p>
                    <dd>Prevádzkovateľ služby je Crew.sk. Cena objednávacej SMS je podľa tarifu zákazníka. Cena prichádzajúcej SMS je 4,50€ s DPH. Technicky zaisťuje ELET, s.r.o., infolinka +421 / 2 / 5479 2179. www.platbamobilom.sk</dd>
                    
                    <div class="medzera-20"></div>

                    <p>SMS formát pre ČR: <b>GSP 118119 99 PRO VIP {meno na serveri}</b></p>
                    <p>Telefónne číslo pre ČR: <b>90210</b></p>
                    <dd>Prevádzkovateľ služby je Crew.sk. Cena objednávacej SMS je podľa tarifu zákazníka. Cena prichádzajúcej SMS je 99,00Kč s DPH. Technicky zaisťuje ORELSOFT.cz, infolinka 491 04 02 01. www.orelsoft.cz</dd>
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        V skutočnej SMS nahraďte <b>{meno na serveri}</b> svojim alebo cudzím herným nickom zo serveru.<br>Príklad obsahu SMS: <b>SERVER PRO 118119 4.5 VIP Polkov</b>.
                    </blockquote>

                    <div class="medzera-20"></div>

                </div>
                <div class="col-md-4">
                    <h3 class="mb-20">Iné spôsoby platby</h3>
                    <p>Prémium hodnosti sa dajú zaplatiť aj cez webové rozhranie PAYU. Stačí vybrať <b>Forma platby: PAYU</b></p>
                    <a href="https://www.crew.sk/vip/market/buy/118119" target="_blank" class="paypal">Zakúpiť cez PAYU</a>
                    <div class="medzera-20"></div>
                    <h3 class="mb-20">QR kód</h3>
                    <p>Pre platbu zo SR môžete tiež naskenovať tento QR kód.</p>
                    <img src="https://www.crew.sk/misc/qr_code/vip_sms/13979?size=100" alt="QR:Code platba">
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        QR kód platí len pre Slovenskú republiku!
                    </blockquote>
                </div>
            </div>

            <div class="medzera-50" id="SVIP"></div>

            <h2 style="color: #a300a3">SuperVIP</h2>
            <div class="medzera-20"></div>
            <p>SuperVIP je ďalší stupeň z našich VIP rankov. Jedná sa teda o obsahovo výraznejšiu hodnosť s hlavnou výhodou prístupu k príkazom /enchant a /enderchest.</p>
            
            <div class="medzera-20"></div>

            <div class="flexx">
                <div class="col-md-6" style="margin-right: 190px;">

                    <h3 class="mb-20">SMS Platba</h3>
                    <p>SMS formát pre SR: <b>SERVER PRO 118119 8 SVIP {meno na serveri}</b></p>
                    <p>Telefónne číslo pre SR: <b>8866</b></p>
                    <dd>Prevádzkovateľ služby je Crew.sk. Cena objednávacej SMS je podľa tarifu zákazníka. Cena prichádzajúcej SMS je 8,00€ s DPH. Technicky zaisťuje ELET, s.r.o., infolinka +421 / 2 / 5479 2179. www.platbamobilom.sk</dd>
                    
                    <div class="medzera-20"></div>

                    <p>SMS formát pre ČR: <b>GSP 118119 249 PRO SVIP {meno na serveri}</b></p>
                    <p>Telefónne číslo pre ČR: <b>90210</b></p>
                    <dd>Prevádzkovateľ služby je Crew.sk. Cena objednávacej SMS je podľa tarifu zákazníka. Cena prichádzajúcej SMS je 249,00Kč s DPH. Technicky zaisťuje ORELSOFT.cz, infolinka 491 04 02 01. www.orelsoft.cz</dd>
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        V skutočnej SMS nahraďte <b>{meno na serveri}</b> svojim alebo cudzím herným nickom zo serveru.<br>Príklad obsahu SMS: <b>SERVER PRO 118119 8 SVIP Polkov</b>.
                    </blockquote>

                    <div class="medzera-20"></div>

                </div>
                <div class="col-md-4">
                    <h3 class="mb-20">Iné spôsoby platby</h3>
                    <p>Prémium hodnosti sa dajú zaplatiť aj cez webové rozhranie PAYU. Stačí vybrať <b>Forma platby: PAYU</b></p>
                    <a href="https://www.crew.sk/vip/market/buy/118119" target="_blank" class="paypal2">Zakúpiť cez PAYU</a>
                    <div class="medzera-20"></div>
                    <h3 class="mb-20">QR kód</h3>
                    <p>Pre platbu zo SR môžete tiež naskenovať tento QR kód.</p>
                    <img src="https://www.crew.sk/misc/qr_code/vip_sms/13982?size=100" alt="QR:Code platba">
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        QR kód platí len pre Slovenskú republiku!
                    </blockquote>
                </div>
            </div>
            
            <div class="medzera-50" id="sposor"></div>

            <h2 style="color: #b17600">Sponsor</h2>
            <div class="medzera-20"></div>
            <p>Hodnosť Sponsor je najvyššia forma odmeny za podporu nášho portálu. Okrem SuperVIP výhod vám táto hodnosť umožní prístup k "epickým" príkazom, ktorý chce každý mať!</p>
            
            <div class="medzera-20"></div>

            <div class="flexx">
                <div class="col-md-6" style="margin-right: 190px;">

                    <h3 class="mb-20">SMS Platba</h3>
                    <p>SMS formát pre SR: <b>SERVER PRO 118119 15 SPONSOR {meno na serveri}</b></p>
                    <p>Telefónne číslo pre SR: <b>8866</b></p>
                    <dd>Prevádzkovateľ služby je Crew.sk. Cena objednávacej SMS je podľa tarifu zákazníka. Cena prichádzajúcej SMS je 15,00€ s DPH. Technicky zaisťuje ELET, s.r.o., infolinka +421 / 2 / 5479 2179. www.platbamobilom.sk</dd>
                    
                    <div class="medzera-20"></div>

                    <blockquote class="generic-blockquote-warn">
                        Platba pomocou SMS z Českej republiky momentálne nie je možná.
                    </blockquote>
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        V skutočnej SMS nahraďte <b>{meno na serveri}</b> svojim alebo cudzím herným nickom zo serveru.<br>Príklad obsahu SMS: <b>SERVER PRO 118119 15 SPONSOR Polkov</b>.
                    </blockquote>

                    <div class="medzera-20"></div>

                </div>
                <div class="col-md-4">
                    <h3 class="mb-20">Iné spôsoby platby</h3>
                    <p>Prémium hodnosti sa dajú zaplatiť aj cez webové rozhranie PAYU. Stačí vybrať <b>Forma platby: PAYU</b></p>
                    <a href="https://www.crew.sk/vip/market/buy/118119" target="_blank" class="paypal3">Zakúpiť cez PAYU</a>
                    <div class="medzera-20"></div>
                    <h3 class="mb-20">QR kód</h3>
                    <p>Pre platbu zo SR môžete tiež naskenovať tento QR kód.</p>
                    <img src="https://www.crew.sk/misc/qr_code/vip_sms/13985?size=100" alt="QR:Code platba">
                    <div class="medzera-20"></div>
                    <blockquote class="generic-blockquote">
                        QR kód platí len pre Slovenskú republiku!
                    </blockquote>
                </div>
            </div>
        </div>
        
    </section>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-title">
                                    <h3>O nás</h3>
                                </div>
                            <div class="text">Sme najlepší herný portál na SK/CZ scéne. Tešiť sa u nás môžte na CS:GO a Rocket League ligy a súťaže, minecraft server s mnoho súťažmi o vip a super vip, dungeons súťaže a mnoho ďalšieho.</div>
							<!--Newsletter-->
							<div class="newsletter-form">
								<form method="post" action="contact.php">
									<div class="form-group clearfix">
										<input type="email" name="email" value="" placeholder="Email adresa" required>
										<button type="submit" class="theme-btn newsletter-btn"><span class="icon flaticon-arrows-6"></span></button>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-5 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Preskúmaj</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="about.php">O nás</a></li>
                                            <li><a href="servers.php">Servery</a></li>
                                            <li><a href="team.php">Team</a></li>
                                            <li><a href="vip.php">VIP</a></li>
                                            <li><a href="https://discord.gg/vnPFP42DUA" target="_blank">Discord</a></li>
                                            <li><a href="faq.php">Faq</a></li>
                                            <li><a href="contact.php">Kontakt</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Podmienky</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="rules.php">Pravidlá</a></li>
                                            <li><a href="gdpr.php">GDPR</a></li>
                                            <li><a href="cookies.php">Cookies</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget info-widget">
                            <ul class="contact-info">
                                <li><strong>Poskytovateľ</strong> <br>Universal Technologies s. r. o.</li>
                                <li><strong>Adresa</strong> <br>Trieda SNP 1534/22 <br>Banská Bystrica, SVK</li>
                                <li><strong>IČO</strong> <br>53 058 968</li>
                                <li><strong>Email</strong> <br><a href="">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="outer-container">
                <!--Footer Bottom Shape-->
                <div class="bottom-shape-box"><div class="bg-shape"></div></div>

                <div class="auto-container">
                    <!--Scroll to top-->
                    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>
                    <!--Scroll to top-->
                    <div class="row clearfix">
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="copyright"><span class="logo-icon"></span> &copy; Copyrights 2020 - 2021 <a href="#">HernáZóna.eu</a> - All Rights Reserved</div>
                        </div>
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="social-links">
                                <ul class="default-social-links">
                                    <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </footer>
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/script.js"></script>

</body>
</html>