<!DOCTYPE html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ21QB3167"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ21QB3167');
</script>
<meta charset="utf-8">
<title>HernáZóna.eu | Team</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">



<link rel="shortcut icon" href="images/hzlogodogo2.png" type="image/x-icon">
<link rel="icon" href="images/hzlogodogo2.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-two">
        <div class="header-container">

            <!-- Header Top 
            <div class="header-top">
            	<div class="auto-container">
                    <div class="inner clearfix">
                        <div class="top-left">
                            <div class="top-text">Vitajte na portáli HernáZóna.eu</div>
                        </div>
        
                        <div class="top-right">
                            <ul class="info clearfix">
                                <li><a href="#">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Header Upper -->
            <div class="header-upper">
            	<div class="auto-container">
                    <div class="inner-container clearfix">
                        <!--Logo-->
                        <div class="logo-box">
                            <div class="logo"><a href="index.php" title=""><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                        </div>
    
                        <!--Nav Box-->
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span></div>
    
                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="index.php">Domov</a></li>
                                        <li><a href="about.php">O nás</a></li>
                                        <li class="current"><a href="servers.php">Servery</a></li>
                                        <!-- <li><a href="gallery.php">Galéria</a></li> -->
                                        <li><a href="team.php">Team</a></li>
                                        <li><a href="vip.php">VIP</a></li>
                                        <li><a href="faq.php">Faq</a></li>
                                        <li class="dropdown"><a href="rules.php">Podmienky</a>
                                            <ul>
                                                <li><a href="rules.php">Pravidlá</a></li>
                                                <li><a href="gdpr.php">GDPR</a></li>
                                                <li><a href="cookies.php">Cookies</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.php">Kontakt</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            <ul class="social-links clearfix">
                                <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--End Header Upper-->
        </div><!--End Header Container-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/hzlogodogo2.png" alt="" title="">hernázóna.eu</a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix top5">
                        <!--Javascript Load-->
                    </nav><!-- Main Menu End-->

                    <ul class="social-links clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                <div class="menu-outer"><!--Javascript Load--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->


    <!--Page Title-->
    <section class="page-banner" style="background-image:url(images/main-slider/huh.jpg);">
        <div class="top-pattern-layer"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>Naše servery</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Banner-->

    <!--Faq Section-->
    <section class="servers-section">
        <div class="top-pattern-layer"></div>

        <div class="auto-container">

            <?php
	            use MCServerStatus\MCPing;

	            include 'src/MCPing.php';
	
	            require_once('src/autoload.php');
	
                $response=MCPing::check('mc.hernazona.eu', 25583);
                
                if($response->players == 0) {
                    $povedz = 'Práve hrá <b style="color: red;">0</b> hráčov';
                } else if($response->players == 1) {
                    $povedz = 'Práve hrá <b style="color: green;">'. $response->players . '</b> hráč';
                } else if($response->players > 1 && $response->players < 4) {
                    $povedz = 'Práve hrajú <b style="color: green;">'. $response->players . '</b> hráči';
                } else {
                    $povedz = 'Práve hrá <b style="color: green;">'. $response->players . '</b> hráčov';
                }

                $status = $response->online ? '<b style="color: green;">online</b>' : '<b style="color: red;">offline</b>';
            ?>

            <div class="sec-title"><h2>Minecraft</h2><span class="bottom-curve"></span></div>
            <div class="contr-server">
                <div class="server wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="server-header">
                        <h2>Survival / Parkour / Dungeons</h2>
                    </div>
                    <div class="server-info-left">
                        <i class="fas fa-plug"></i>
                        <?php echo $status ?>
                    </div>
                    <div class="server-info-right">
                        <i class="fas fa-gamepad"></i>
                        <?php echo $povedz ?>
                    </div>
                    <div class="server-description">
                        <p>Na tomto serveri bude chcieť hrať každý z vás. Môžeš tu stavať v normálnom svete, ak ti chýbajú suroviny alebo potrebuješ nejaké epické veci, môžeš ich získať lootením Dungeonov. Ak sa cítiš na parkour a preskáčeš sa do cieľa, odmena je pre teba istá! Odmeny využiješ samozrejme v normálnom svete. Čaká ťa tu leaderboard najlepších hráčov z parkouru, ľahké až náročné Dungeony, kopec zábavy a výborná komunita!</p>
                    </div>
                    <div class="server-ip">
                        <i class="fas fa-server"></i>
                        <b>mc.hernazona.eu</b>
                    </div>
                </div>
                <div class="server wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="server-header">
                        <h2>SkyBlock</h2>
                    </div>
                    <div class="server-info-left">
                        <i class="fas fa-plug"></i>
                        <b>v príprave</b>
                    </div>
                    <div class="server-info-right">
                        <i class="fas fa-gamepad"></i>
                        Práve hrá
                        <b>0</b>
                        hráčov.
                    </div>
                    <div class="server-description">
                        <div class="skills-container">
                            <div class="skills mc">50%</div>
                        </div>
                    </div>
                </div>
                <div class="server wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="server-header">
                        <h2>Creative</h2>
                    </div>
                    <div class="server-info-left">
                        <i class="fas fa-plug"></i>
                        <b>v príprave</b>
                    </div>
                    <div class="server-info-right">
                        <i class="fas fa-gamepad"></i>
                        Práve hrá
                        <b>0</b>
                        hráčov.
                    </div>
                    <div class="server-description">
                        <div class="skills-container">
                            <div class="skills mc2">0%</div>
                        </div>
                    </div>
                </div>
                <div class="server wow fadeInLeft" data-wow-delay="800ms" data-wow-duration="1500ms">
                    <div class="server-header">
                        <h2>MiniGames</h2>
                    </div>
                    <div class="server-info-left">
                        <i class="fas fa-plug"></i>
                        <b>v príprave</b>
                    </div>
                    <div class="server-info-right">
                        <i class="fas fa-gamepad"></i>
                        Práve hrá
                        <b>0</b>
                        hráčov.
                    </div>
                    <div class="server-description">
                        <div class="skills-container">
                            <div class="skills mc2">0%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
    </section>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-title">
                                    <h3>O nás</h3>
                                </div>
                            <div class="text">Sme najlepší herný portál na SK/CZ scéne. Tešiť sa u nás môžte na CS:GO a Rocket League ligy a súťaže, minecraft server s mnoho súťažmi o vip a super vip, dungeons súťaže a mnoho ďalšieho.</div>
							<!--Newsletter-->
							<div class="newsletter-form">
								<form method="post" action="contact.php">
									<div class="form-group clearfix">
										<input type="email" name="email" value="" placeholder="Email adresa" required>
										<button type="submit" class="theme-btn newsletter-btn"><span class="icon flaticon-arrows-6"></span></button>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-5 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Preskúmaj</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="about.php">O nás</a></li>
                                            <li><a href="servers.php">Servery</a></li>
                                            <li><a href="team.php">Team</a></li>
                                            <li><a href="vip.php">VIP</a></li>
                                            <li><a href="https://discord.gg/vnPFP42DUA" target="_blank">Discord</a></li>
                                            <li><a href="faq.php">Faq</a></li>
                                            <li><a href="contact.php">Kontakt</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Podmienky</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="rules.php">Pravidlá</a></li>
                                            <li><a href="gdpr.php">GDPR</a></li>
                                            <li><a href="cookies.php">Cookies</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget info-widget">
                            <ul class="contact-info">
                                <li><strong>Poskytovateľ</strong> <br>Universal Technologies s. r. o.</li>
                                <li><strong>Adresa</strong> <br>Trieda SNP 1534/22 <br>Banská Bystrica, SVK</li>
                                <li><strong>IČO</strong> <br>53 058 968</li>
                                <li><strong>Email</strong> <br><a href="">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="outer-container">
                <!--Footer Bottom Shape-->
                <div class="bottom-shape-box"><div class="bg-shape"></div></div>

                <div class="auto-container">
                    <!--Scroll to top-->
                    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>
                    <!--Scroll to top-->
                    <div class="row clearfix">
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="copyright"><span class="logo-icon"></span> &copy; Copyrights 2020 - 2021 <a href="#">HernáZóna.eu</a> - All Rights Reserved</div>
                        </div>
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="social-links">
                                <ul class="default-social-links">
                                    <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </footer>
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/script.js"></script>

</body>
</html>