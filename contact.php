<!DOCTYPE html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ21QB3167"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ21QB3167');
</script>
<script src="https://www.google.com/recaptcha/api.js?render=6LeAWSQaAAAAANloyiHgxxPVodW5B6ab4CIU5eO3"></script>
<meta charset="utf-8">
<title>HernáZóna.eu | Kontakt</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">



<link rel="shortcut icon" href="images/hzlogodogo2.png" type="image/x-icon">
<link rel="icon" href="images/hzlogodogo2.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-two">
        <div class="header-container">

            <!-- Header Top 
            <div class="header-top">
            	<div class="auto-container">
                    <div class="inner clearfix">
                        <div class="top-left">
                            <div class="top-text">Vitajte na portáli HernáZóna.eu</div>
                        </div>
        
                        <div class="top-right">
                            <ul class="info clearfix">
                                <li><a href="#">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Header Upper -->
            <div class="header-upper">
            	<div class="auto-container">
                    <div class="inner-container clearfix">
                        <!--Logo-->
                        <div class="logo-box">
                            <div class="logo"><a href="index.php" title=""><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                        </div>
    
                        <!--Nav Box-->
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span></div>
    
                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="index.php">Domov</a></li>
                                        <li><a href="about.php">O nás</a></li>
                                        <li><a href="servers.php">Servery</a></li>
                                        <!-- <li><a href="gallery.php">Galéria</a></li> -->
                                        <li><a href="team.php">Team</a></li>
                                        <li><a href="vip.php">VIP</a></li>
                                        <li><a href="faq.php">Faq</a></li>
                                        <li class="dropdown"><a href="rules.php">Podmienky</a>
                                            <ul>
                                                <li><a href="rules.php">Pravidlá</a></li>
                                                <li><a href="gdpr.php">GDPR</a></li>
                                                <li><a href="cookies.php">Cookies</a></li>
                                            </ul>
                                        </li>
                                        <li class="current"><a href="contact.php">Kontakt</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            <ul class="social-links clearfix">
                                <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--End Header Upper-->
        </div><!--End Header Container-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/hzlogodogo2.png" alt="" title="">hernázóna.eu</a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix top5">
                        <!--Javascript Load-->
                    </nav><!-- Main Menu End-->

                    <ul class="social-links clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                <div class="menu-outer"><!--Javascript Load--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->

    <!--Page Title-->
    <section class="page-banner" style="background-image:url(images/main-slider/huh.jpg);">
        <div class="top-pattern-layer"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>Kontakt</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Banner-->

    <?php

        use PHPMailer\PHPMailer\PHPMailer;

        require 'src/autoload.php';

        if(array_key_exists('submit-form', $_POST)) {
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $data = [
                'secret' => "6LeAWSQaAAAAAINSMvJ4tFIr1oTcnqleGkElamfY",
                'response' => $_POST['token'],
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ];
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context = stream_context_create($options);
            $response = file_get_contents($url, false, $context);
            $res = json_decode($response, true);

            $err = false;
            $msg = '';
            $email = '';
            if(array_key_exists('meno', $_POST)) {
                $meno = substr(strip_tags($_POST['meno']), 0, 255);
            }

            if(array_key_exists('nick', $_POST)) {
                $nick = substr(strip_tags($_POST['nick']), 0, 255);
            }

            if(array_key_exists('email', $_POST) && PHPMailer::validateAddress($_POST['email'])) {
                $email = $_POST['email'];
            } else {
                $msg .= 'Chyba: zadaj správnu emailovú adresu!';
                $err = true;
            }

            if(array_key_exists('message', $_POST)) {
                $message = substr(strip_tags($_POST['message']), 0, 16384);
            }

            if(!empty($_POST['fullname'])) {
                $err = true;
            }

            if($res['success'] == true && $res['score'] > 0.5 && !$err) {
                $mail = new PHPMailer();
                $mail->isSMTP();
                $mail->Host = 'smtp.hostcreators.sk';
                $mail->SMTPAuth = true;
                $mail->Username = 'david@hernazona.eu';
                $mail->Password = 'L!Eao4Hc-_51';
                $mail->Port = 25;
                $mail->CharSet = PHPMailer::CHARSET_UTF8;
                $mail->setFrom('david@hernazona.eu');
                $mail->addAddress('info@hernazona.eu');
                $mail->addCC('dadkop@gmail.com');
                $mail->addReplyTo($email, $meno);
                $mail->Subject = 'Kontaktný formulár hernazona.eu';
                $mail->Body = $message;
                
                if(!$mail->send()) {
                    $msg .= 'Mail chyba: ' . $mail->ErrorInfo;
                } else {
                    $msg .= 'Správa odoslaná!';
                }
            }
        }

    ?>

    <!--Contact Section-->
    <section class="contact-section">
        <div class="auto-container">
            <!--Title-->
            <div class="sec-title centered"><h2>Napíšte nám</h2><span class="bottom-curve"></span></div>

            <div class="form-box">
                <div class="default-form contact-form">
                <?php if (empty($msg)) { ?>
                    <form method="post" id="contact-form">
                        <div class="row clearfix">                                    
                            <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                <input type="text" name="meno" placeholder="Meno" required="">
                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12 form-group">
                                <input type="text" name="nick" placeholder="Nick v hre" required="">
                            </div>
                            
                            <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                <input type="email" name="email" placeholder="Email adresa" required="">
                            </div>

                            <div class="col-md-12 col-sm-12 form-group">
                                <textarea name="message" placeholder="Napíš správu"></textarea>
                            </div>

                            <input type="hidden" name="fullname"/>

                            <div class="col-md-12 col-sm-12 form-group">
                                <div class="text-center">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form"><span class="btn-title">Poslať</span></button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="token" name="token">
                    </form>
                <?php } else {
                    echo '<h2 class="centered">' . $msg . '</h2>';
                } ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-title">
                                    <h3>O nás</h3>
                                </div>
                            <div class="text">Sme najlepší herný portál na SK/CZ scéne. Tešiť sa u nás môžte na CS:GO a Rocket League ligy a súťaže, minecraft server s mnoho súťažmi o vip a super vip, dungeons súťaže a mnoho ďalšieho.</div>
							<!--Newsletter-->
							<div class="newsletter-form">
								<form method="post" action="contact.php">
									<div class="form-group clearfix">
										<input type="email" name="email" value="" placeholder="Email adresa" required>
										<button type="submit" class="theme-btn newsletter-btn"><span class="icon flaticon-arrows-6"></span></button>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-5 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Preskúmaj</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="about.php">O nás</a></li>
                                            <li><a href="servers.php">Servery</a></li>
                                            <li><a href="team.php">Team</a></li>
                                            <li><a href="vip.php">VIP</a></li>
                                            <li><a href="https://discord.gg/vnPFP42DUA" target="_blank">Discord</a></li>
                                            <li><a href="faq.php">Faq</a></li>
                                            <li><a href="contact.php">Kontakt</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Podmienky</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="rules.php">Pravidlá</a></li>
                                            <li><a href="gdpr.php">GDPR</a></li>
                                            <li><a href="cookies.php">Cookies</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget info-widget">
                            <ul class="contact-info">
                                <li><strong>Poskytovateľ</strong> <br>Universal Technologies s. r. o.</li>
                                <li><strong>Adresa</strong> <br>Trieda SNP 1534/22 <br>Banská Bystrica, SVK</li>
                                <li><strong>IČO</strong> <br>53 058 968</li>
                                <li><strong>Email</strong> <br><a href="">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="outer-container">
                <!--Footer Bottom Shape-->
                <div class="bottom-shape-box"><div class="bg-shape"></div></div>

                <div class="auto-container">
                    <!--Scroll to top-->
                    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>
                    <!--Scroll to top-->
                    <div class="row clearfix">
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="copyright"><span class="logo-icon"></span> &copy; Copyrights 2020 - 2021 <a href="#">HernáZóna.eu</a> - All Rights Reserved</div>
                        </div>
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="social-links">
                                <ul class="default-social-links">
                                    <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </footer>
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/validate.js"></script>
<script src="js/script.js"></script>

<!--Google Map APi Key-->
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->

<!--Google ReCaptcha Callback-->
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LeAWSQaAAAAANloyiHgxxPVodW5B6ab4CIU5eO3', {action: 'homepage'}).then(function(token) {
        document.getElementById("token").value = token;
    });
});
</script>
<!--End Google ReCaptcha Callback-->

</body>
</html>