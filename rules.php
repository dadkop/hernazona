<!DOCTYPE html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ21QB3167"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ21QB3167');
</script>
<meta charset="utf-8">
<title>HernáZóna.eu | Pravidlá</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">



<link rel="shortcut icon" href="images/hzlogodogo2.png" type="image/x-icon">
<link rel="icon" href="images/hzlogodogo2.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-two">
        <div class="header-container">

            <!-- Header Top 
            <div class="header-top">
            	<div class="auto-container">
                    <div class="inner clearfix">
                        <div class="top-left">
                            <div class="top-text">Vitajte na portáli HernáZóna.eu</div>
                        </div>
        
                        <div class="top-right">
                            <ul class="info clearfix">
                                <li><a href="#">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Header Upper -->
            <div class="header-upper">
            	<div class="auto-container">
                    <div class="inner-container clearfix">
                        <!--Logo-->
                        <div class="logo-box">
                            <div class="logo"><a href="index.php" title=""><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                        </div>
    
                        <!--Nav Box-->
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span></div>
    
                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="index.php">Domov</a></li>
                                        <li><a href="about.php">O nás</a></li>
                                        <li><a href="servers.php">Servery</a></li>
                                        <!-- <li><a href="gallery.php">Galéria</a></li> -->
                                        <li><a href="team.php">Team</a></li>
                                        <li><a href="vip.php">VIP</a></li>
                                        <li><a href="faq.php">Faq</a></li>
                                        <li class="dropdown current"><a href="rules.php">Podmienky</a>
                                            <ul>
                                                <li><a href="rules.php">Pravidlá</a></li>
                                                <li><a href="gdpr.php">GDPR</a></li>
                                                <li><a href="cookies.php">Cookies</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.php">Kontakt</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            <ul class="social-links clearfix">
                                <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--End Header Upper-->
        </div><!--End Header Container-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/hzlogodogo2.png" alt="" title="">hernázóna.eu</a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix top5">
                        <!--Javascript Load-->
                    </nav><!-- Main Menu End-->

                    <ul class="social-links clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                <div class="menu-outer"><!--Javascript Load--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->

    <!--Page Title-->
    <section class="page-banner" style="background-image:url(images/main-slider/huh.jpg);">
        <div class="top-pattern-layer"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>Pravidlá</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Banner-->

    <section class="servers-section">
        <div class="top-pattern-layer"></div>

        <div class="auto-container">
            <div class="sec-title centered"><h2>Oficiálne pravidlá portálu HernáZóna.eu</h2><span class="bottom-curve"></span></div>
            <p><h3><b>§1 Všeobecné pravidlá</b></h3></p>
            <ol class="list2">
                <li>Registráciou na portáli HernáZóna.eu súhlasíte so všetkými pravidlami a GDPR.</li>
                <li>Na všetkých serveroch a službách s ním súvisiacich, ktoré spadajú pod portál HernáZóna.eu, je zakázané umiestňovať reklamy v akomkoľvek zmysle.</li>
                <li>Hráč má právo na pripomienku, ktorá sa týka serverov HernáZóna.eu.</li>
                <li>Pokiaľ hráč poruší pravidlá, bude potrestaný podľa banovacej tabuľky dostupnej pre Admin Team, a podľa uváženia člena teamu, ktorý bude hráča trestať (tresty sú nenásilné).</li>
                <li>Je zakázané obchádzať trest.</li>
                <li>Pri udeľovaní trestu je člen teamu povinný uviesť dôvod trestu.</li>
                <li>Dôvod "<b>blacklist</b>" obmedzí prístup na všetko spojené s portálom HernáZóna.eu. Tento trest má právo udeliť len majiteľ portálu HernáZóna.eu. Majiteľ má právo upraviť akúkoľvek časť blacklistu, vytvoriť blacklist bez udania dôvodu alebo hráča odbanovať. Hráč <b>nemá právo</b> sa pre tento druh trestu odvolať podaním žiadosti o unban.</li>
                <li>Proti trestu sa môže hráč odvolať len podaním žiadosti o unban/unmute na našom discorde (nikde inde), ktorý musí byť vyplnení podľa vzoru. Hráč má právo riešiť len svoj trest. V opačnom prípade bude žiadosť automaticky zamietnutá.</li>
                <li>Pokiaľ bude hráč potrestaný, všetko, čo na portál prispel (VIP, finančné príspevky...), vybudoval, vyhral a pod. mu nebude nijak vrátené.</li>
                <li>Pravidlá sa môžu kedykoľvek zmeniť bez udania dovôdu a upozornenia.</li>
                <li>Všetky dáta portálu sú výhradne majetkom portálu HernáZóna.eu a pri akomkoľvek neúmyselnom (aj úmyselnom) poškodení alebo strate týchto dát, nemá hráč právo na náhradu škôd.</li>
                <li>Je zakázané používať herný nick, ktorý sa akokoľvek podobá nicku člena admin tímu.</li>
                <li>Na všetkých službách portálu HernáZóna.eu je hráč povinný používať rovnaký alebo veľmi podobný nick.</li>
                <li>Je zakázané navádzať hráčov k porušovaniu pravidiel.</li>
                <li>Je zakázané navádzať hráčov k používaniu príkazov, alebo klávesovách skratiek, ktoré im môžu akokoľvek uškodiť.</li>
                <li>Vo všetkých službách portálu HernáZóna.eu sa zakazuje v chatoch používať iný jazyk ako slovenčina, čeština a angličtina.</li>
                <li>Je zakázané vlastniť viac než 1 herný účet (nick) za účelom získania výhod alebo obchádzania trestu.</li>
                <li>Nikto nemá právo po niekom vyžadovať prihlasovacie údaje. V prípade poskytnutia týchto údajov tretej osobe, alebo v prípade prelomenia vášho hesla nenesieme žiadnu zodpovednosť za prípadné škody. Volte vždy silné heslá.</li>
                <li>Warez hráčom, teda hráčom, ktorí nemajú zakúpené origináne verzie hry, nebude poskytnutá žiadna podpora v prípade straty hesla či použitia nicku reálnym vlastníkom tohto nicku, teda hráčom, ktorý má na tento nick zakúpenú orignálnu verziu hry.</li>
                <li>Je zakázané akokoľvek poškodzovať servery, využívať bugy v hre a získať tak pre seba výhodu. Všetky bugy, o ktorých si je hráč vedomý, je povinný nahlásiť na náš discord.</li>
                <li>Pokiaľ je hráč neplnoletý, za jeho činy zodpovedá zákonný zástupca.</li>
                <li>Je zakázané akýmkoľvek spôsobom obchodovať s výhodami na portáli, hernými účtami, hernou menou a predmetmi za akúkoľvek inú menu než je herná a ekonomická mena portálu HernáZóna.eu.</li>
                <li>Dôkazným materiálom sa rozumie screenshot alebo video, ktoré je dobre posudzovateľné a zobrazuje dátum a čas udalosti. Dôkaz starší než 7 dní od udalosti, na ktorú odkazuje, je neplatný.</li>
                <li>Je prísne zakázané vytvárať a používať falošné dôkazy.</li>
                <li>Je zakázané akékoľvek neslušné, vulgárne, rasistické, úchylné či sexuálne a neetické chovanie a vyjadrovanie priamo na sexuálnu tématiku akýmkoľvek spôsobom.</li>
                <li>Je zakázané obviňovať hráčov z porušovania pravidiel, pokiaľ neexistuje jasný dôkaz. Pre nahlásenie hráča slúžia discord tickety, nie verejný alebo súkromný chat s členom tímu.</li>
                <li>Je zakázané písať nevyžiadané správy za účelom zahltiť chat (spam) ako vo verejných chatoch tak v súkromných chatoch, ktoré sú súčasťou portálu HernáZóna.eu. Výnimka je pre jednotlivú správu s odstupom viac ako 10 sekúnd.</li>
                <li>Je prísne zakázané hľadať a zneužívať diery (chyby), poprípade prekrúcanie významu určitých slov v pravidlách.</li>
                <li>Je zakázané vytváranie tzv. "fake" súťaží na serveroch a službách s ním spojených.</li>
                <li>Je zakázané používať nepovolené modifikácie hier (tzv. cheaty). "Hacknuté" clienty a akékoľvek modifikácie, ktoré akokoľvek zvýhodňujú hráča proti ostatným hráčom. Ostatné modifikácie a clienty používa hráč na vlastné nebezpečenstvo.</li>
                <li>Je zakázané využívať autoclickery, akékoľvek nastavenia myši/v hre alebo akékoľvek zablokovanie tlačítka myši, ktoré umožňujú automatické klikanie či držanie tlačítka behom neprítomnosti v hre.</li>
            </ol>
            <p><h3><b>§2 Pravidlá jednotlivých serverov</b></h3></p>
            <p><h4><b><i>Minecraft</i></b></h4></p>
            <p><h5 style="margin-left: 20px;"><b>Survival / Parkour / Dungeons</b></h5></p>
            <ol class="list2">
                <li>Je zakázané griefovať, ničiť prírodu a stavby iných hráčov. Za neoreskovanú rozgriefovanú oblasť nenesieme žiadnu zodpovednosť. Rozgriefovaná oblasť sa dá obvykle vrátiť do 24 hodín od zničenia.</li>
                <li>Admin tím nie je povinný vrátiť hráčovi rozgriefovanú oblasť pokiaľ k tomu nemá prostriedky.</li>
                <li>Verejné farmy a celkové teleporty nesmú nijak poškodzovať hráčov ak to pri zverejnení nebolo oznámené.</li>
                <li>PvP je povolené bez povinnosti vrátenia predmetov, pokiaľ už predom nebolo dohodnuté inak.</li>
                <li>Je zakázané strielať (luk a pod.) z non-pvp zóny.</li>
                <li>Pri odpojení alebo pádu hry v priebehu boja či letu nenesieme žiadnu zodpovednosť za stratu vašich predmetov.</li>
                <li>Je zakázané vlastniť predmety, ktoré nejde získať obyčajným hraním alebo zakúpením. Pokiaľ narazíte na akýkoľvek predmet, ktorý do hry nepatrí, je hráč povinný ho odovzdať admin tímu.</li>
                <li>V prípade obchodu s iným hráčom a následným okradnutím má hráč právo na návrat predmetov. Musí k tomu mať však dostatočné množstvo dôkazov, preto vždy doporučujeme si v priebehu "tradovania" robiť záznam obrazovky alebo obrazovku fotiť.</li>
                <li>Je zakázané obmedzovať alebo ničiť okolie cudzej rezidencie.</li>
                <li>Je zakázané vlastniť oblasť, ktorá akokoľvek spôsobuje lagovanie serveru. Pri porušení bude zdroj lagu odstránený bez výzvy a náhrady.</li>
                <li>Je prísne zakázaný tpakill, čiže zaslanie žiadosti o teleport alebo požiadanie o zaslanaia teleportu k hráčovi a následné zavraždenie hráča, pokiaľ o tom hráč nie je predom informovaný.</li>
                <li>Je zakázané kicknutie/vylúčenie hráča z rezidencie a následné privlastnenie jeho predmetov v truhlách, ... bez ich navátenia.</li>
                <li>Je zakázané boostenie (posilňovanie) účtov (killy, exp, ...) pomocou iného hráča či alternatívneho účtu.</li>
                <li>Je zakázané stavať si domy a hrať normálnu hru v Dungeon svete. Tento svet je určený na lootenie a bude sa resetovať podľa potreby.</li>
                <li>Je zakázané akékoľvek bránenie hráčom v postupe hry (parkour oblasť).</li>
                <li>Je zakázané dokončovať levely iným spôsobom alebo cestou, než ktorá k tomu bola určená (parkour oblasť).</li>
                <li>Používanie príkazu /fly hráčmi s VIP výhodami v parkour oblasti vyústi v okamžitý ban.</li>
            </ol>
            <p><h5 style="margin-left: 20px;"><b>Skyblock</b></h5></p>
            <ol class="list2">
                <li>Pre skyblock server platia všetky pravidlá serveru <b>Survival / Parkour / Dungeons</b> okrem pravila <b>9</b>, <b>14</b> a <b>15</b>.</li>
                <li>Je zakázané kicknutie hráča z ostrova a následné privlastnenie jeho predmetov v truhlách, ... bez ich navrátenia.</li>
                <li>Je zakázané vlastniť ostrov, ktorý má nevhodný či zavádzajúci obsah. Takýto ostrov bude bez upozornenia a vrátenia predmetov zmazaný.</li>
            </ol>
            <p><h5 style="margin-left: 20px;"><b>Minigames</b></h5></p>
            <ul class="list">
                <li><b>V príprave.</b></li>
            </ul>
            <p><h4><b><i>Rocket League</i></b></h4></p>
            <p><h5 style="margin-left: 20px;"><b>Rocket League WindCup Liga</b></h5></p>
            <ol class="list2">
                <li>3v3 / Single Elimination / Online Turnaj</li>
                <li>Koná sa 27. februára 15:00 CET.</li>
                <li>Registrácia končí 26. februára 23:59 CET.</li>
                <li>Check-in 27. februára od 12:00 do 14:00.</li>
                <li>Európske servery.</li>
                <li>Pred riadnou registráciou musia byť všetci hráči na našom Discord serveri a dostať tímovú rolu.</li>
                <li>Po registrácii musíte uviesť svoje tímové logo (ak ním disponujete), názov tímu a svoje 2 tímové farby na našom Discorde (PM Polkov#3989).</li>
                <li>Na vašom zozname sú povolení 3 hráči a 3 náhradníci. Ak máte viac, musíte vytvoriť B tím a mať ich tam.</li>
                <li>Žiadne 2v3, takže ak vo vašom tíme nie je dostatok ľudí, jedná sa o automatickú diskvalifikáciu (to znamená, že diskvalifikovaný bude iba tím s nedostatkom hráčov, ak oba tími nemajú dostatok hráčov, oba tími budú diskvalifikované).</li>
                <li>Ak zápas mešká desať minút, tím spôsobujúci zdržanie bude automaticky diskvalifikovaný.</li>
                <li>Organizácia ligy prebieha výlučne na našom Discord serveri.</li>
                <li>Registráciu zabezpečuje portál <a href="https://www.toornament.com/en_US/" target="_blank">toornament.com</a>. Registráciou <b>súhlasíš</b> s ich podmienkami používania.</li>
                <li>Kvalifikácia pozostáva zo 4. kvalifikačných kôl, na konci ktorých sa do hlavnej sezóny kvalifikujú prvé 3 tími.</li>
                <li>Už kvalifikované tími sa nesmú zúčastniť ďalších kvalifikačných kôl.</li>
                <li>Na konci kvalifikačných kôl zíde 12 tímov, ktoré postupujú do hlavnej sezóny.</li>
                <li>Hlavná sezóna sa bude konať offline v dohodnutých priestoroch v priebehu mesiaca máj/jún (ak sitácia nedovolí inak, bude sa konať online). Bližšie informácie dostane každý tím v dostatočnom predstihu.</li>
                <li>Z hlavnej sezóny si víťazi odnesú vecné ceny a sponzorské dary od našich sponzorov.</li>
            </ol>
            <p><h4><b><i>Counter Strike: Global Offensive</i></b></h4></p>
            <ul class="list">
                <li><b>V príprave.</b></li>
            </ul>
            <p><h3><b>§3 Pravidlá VIP výhod a všetkých platieb</b></h3></p>
            <ol class="list2">
                <li>Platba za akýkoľvek VIP Rank <b>(ďalej len "výhody")</b> je považovaná ako príspevok na chod a rozvoj portálu. Všetkých zakúpených výhod sa dá dosiahnuť bežným hraním na serveroch (výnimku tvoria niektoré kozmetické výhody).</li>
                <li>Naše servery nie sú žiadnym spôsobom spojené a nie sú súčasťou spoločností s vydavateľmi daných hier.</li>
                <li>To, že si hráč kúpi výhody, neznamená, že nemôže byť nijak potrestaný v súlade s pravidlami.</li>
                <li>Doba trvania po sebe zakúpených výhod sa nesčítava!</li>
                <li>Hráč (kupujúci) nemá právo na vrátenie peňazí. Hráč je povinný si prečítať <b>Obchodné podmienky</b>, <b>Súhlas so spracovaním osobných údajov</b> a taktiež <b>Zoznam spracovateľov osobných údajov</b> tu: <a href="https://www.crew.sk/dokumenty">https://www.crew.sk/dokumenty</a>. Pokiaľ s nimi hráč nesúhlasí, platba nemôže byť z jeho strany zrealizovaná.</li>
                <li>Vedenie portálu si vyhradzuje právo na odobranie výhod za porušenie pravidiel, prípadne pri ukončení celého portálu HernáZóna.eu.</li>
                <li>Zakúpením výhod súhlasíte s pravidlami portálu.</li>
                <li>Vedenie portálu si vyhradzuje právo na zmenu benefitov VIP Rankov.</li>
                <li>Za zlé napísané SMS správy <b>neručíme</b>!</li>
                <li>Výhody nejdú prenášať z jedného nicku na druhý, výnimku tvorí len zmena nicku u originálnych vydavateľov hier alebo preklep pri objednávke.</li>
                <li>Všetky reklamácie sa dajú podať na našom discorde.</li>
                <li>Portál nenesie žiadnu zodpovednosť za to, ako hráč vynaloží s hernou menou, ktorú si zakúpil.</li>
            </ol>
            <p><h3><b>§4 Pravidlá služieb súvisiacich s portálom HernáZóna.eu</b></h3></p>
            <p><h4><b><i>Discord</i></b></h4></p>
            <ol class="list2">
                <li>Je zakázaná propagácia iných discord serverov.</li>
                <li>Je zakázané masovo spamovať mentions.</li>
                <li>Je zakázané odosielať nevhodné tickety a nápady.</li>
                <li>Neposielať nápady, čo sa okolo nápadov ani len nepohybuje.</li>
                <li>Zákaz spamovať chat a voice kanály.</li>
                <li>Zákaz zahlcovať botov.</li>
                <li>Na vyriešenie vašich problémov nám dajte čas, sme len ľudia. Preto nevytvárajte ďalšie tickety a nespamujte zbytočne už vytvorený ticket. Vám ale aj nám to zaberie viac času na vyriešenie problému a zbytočne nás zahltíte rovnakými ticketmi.</li>
                <li>Pokiaľ váš nápad schváli komunita, nemusí sa pridať. Hlavné rozhodnutie padá na vedenie portálu.</li>
                <li>Vo voice miestnostiach je zakázané používať meniče hlasu a púšťať nevhodné zvuky.</li>
                <li>Je zakázané vytvárať tickety, ktoré nie sú určené k riešeniu vášho problému.</li>
                <li>Užívateľ je povinný dodržiavať pravidlá služby <b>Discord</b>.</li>
                <li>Žiadne nadávky na rasismuz, náboženstvo, sexuálnu orientáciu a pod.</li>
                <li>Dodržuj kanály.</li>
                <li>NSFW content správy posielajte striktne do #nsfw.</li>
                <li>Používaj #faq | Na otázky, ktoré sa tu nachádzajú, odpovedať nebudeme.</li>
            </ol>
            <p><h3><b>§5 Práva a povinnosti Admin Teamu a Vedenia portálu HernáZóna.eu</b></h3></p>
            <ol class="list2">
                <li>Člen Admin tímu <b>(ďalej len "Člen AT")</b> je povinný vyriešiť všetky problémy hráčov spravodlivo a v čo najkratšom čase, pokiaľ je to možné. (Neplatí pre Hlavné pozície tímu, Developerov a minecraft Builderov.)</li>
                <li>Člen AT a Vedenie portálu <b>(ďalej len "Vedenie")</b> má právo na svoj osobný život, nie je teda povinný tráviť svoj volný čas na serveroch.</li>
                <li>Člen AT si vyhradzuje maximálne 2 hodiny vkuse na vyriešenie vášho problému v podpore na discorde.</li>
                <li>Člen AT a Vedenie nesmie zneužívať svojich práv akýmkoľvek spôsobom.</li>
                <li>Člen AT a Vedenie majú zakázané poskytovať hráčom akékoľvek výhody, hernú menu a všetko, čo by bolo nespravodlivé voči ostatným hráčom.</li>
                <li>Člen AT je povinný dodržovať všetky pravidlá portálu (neplatí na nedovolené modifikácie pre vyhľadávanie hráčov a pod).</li>
                <li>Člen AT je povinný informovať Vedenie o prevedených zmenách a novinkách na portáli.</li>
                <li>Vedenie je povinné informovať hráčov o prevedených zmenách a novinkách na portáli prostredníctvom changelogu či oznámení (vo väčšine prípadov na Discorde).</li>
                <li>Miesto v Admin Teame nejde zakúpiť.</li>
                <li>Člen AT má zakázané trestať ostatných členov AT. Prehrešky členov AT rieši len Vedenie a Hlavné pozície.</li>
                <li>Člen AT a Vedenie nesmie zverejňovať informácie, ktoré sú určené len pre Admin Team a Vedenie.</li>
                <li>Člen AT má v prípade zamietnutia žiadosti o unban povinnosť uviesť dôvod zamietnutia žiadosti.</li>
                <li>Členstvo v AT či Hlavných pozíciách nie je považované za zamestnanie či brigádu a je členom len na základe svojej dobrovoľnosti.</li>
                <li>Člen AT a Vedenie je povinné dostaviť sa na poradu, pokiaľ je zvolaná (je možné udeliť výnimku).</li>
                <li>Člen AT je povinný ponechať si dôkazy po dobu 7 dní od incidentu, po 7 dňoch nemá hráč právo o požadovanie dôkazov.</li>
            </ol>
            <p><h3><b>§6 Ochrana osobných údajov (GDPR/Privacy Policy)</b></h3></p>
            <p><h4>Jedná sa o doplnenie týchto pravidiel: <a href="https://www.hernazona.eu/gdpr.php">https://www.hernazona.eu/gdpr.php</a></h4></p>
            <ol class="list2">
                <li>Každý je si povinný prečitať spomenuté pravidlá vyššie!</li>
                <li>Všetky zhromažďované dáta zostávajú uložené na portáli HernáZóna.eu, Crew.sk, payu.com a HostCreators.sk. Sú dobre chránené proti odcudzeniu, zneužitiu či strate.</li>
                <li>Nickname hráča slúži na unikátnu identifikáciu hráča a je dostupný pre všetkých užívateľov portálu presne tak isto ako všetky komunikácie medzi hráčmi v chatových miestnostiach a na serveroch.</li>
                <li>Portál navyše ukladá IP adresu, heslá a UUID užívateľov.</li>
                <li>IP adresa slúži k detekovaniu spamu a vytváraniu alternatívnych účtov, k IP adrese majú prístup len poverení členovia Vedenia.</li>
                <li>Heslá sú šifrované modernými technológiami, čiže ich nie je možné prečítať ani vytiahnuť zo systému.</li>
                <li>UUID slúži na identifikáciu clienta.</li>
                <li>Na Discorde nie sú nami zhromažďované žiadne dodatočné osobné dáta okrem vášho nicku.</li>
                <li>Údaje ohľadne zakúpených výhod nemažeme vzhľadom k možným sporom.</li>
                <li>IP adresa blacklistovaných či permanentne zabanovaných užívateľov zostáva uložená a nie je možné zažiadať o jej vymazanie zo systému.</li>
                <li>Užívatelia môžu zdielať screenshoty textových či hlasových správ iných užívateľov bez zvolenia len v prípade, kedy daný užívateľ porušuje pravidlá.</li>
            </ol>
        </div>
    </section>

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-title">
                                    <h3>O nás</h3>
                                </div>
                            <div class="text">Sme najlepší herný portál na SK/CZ scéne. Tešiť sa u nás môžte na CS:GO a Rocket League ligy a súťaže, minecraft server s mnoho súťažmi o vip a super vip, dungeons súťaže a mnoho ďalšieho.</div>
							<!--Newsletter-->
							<div class="newsletter-form">
								<form method="post" action="contact.php">
									<div class="form-group clearfix">
										<input type="email" name="email" value="" placeholder="Email adresa" required>
										<button type="submit" class="theme-btn newsletter-btn"><span class="icon flaticon-arrows-6"></span></button>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-5 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Preskúmaj</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="about.php">O nás</a></li>
                                            <li><a href="servers.php">Servery</a></li>
                                            <li><a href="team.php">Team</a></li>
                                            <li><a href="vip.php">VIP</a></li>
                                            <li><a href="https://discord.gg/vnPFP42DUA" target="_blank">Discord</a></li>
                                            <li><a href="faq.php">Faq</a></li>
                                            <li><a href="contact.php">Kontakt</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Podmienky</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="rules.php">Pravidlá</a></li>
                                            <li><a href="gdpr.php">GDPR</a></li>
                                            <li><a href="cookies.php">Cookies</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget info-widget">
                            <ul class="contact-info">
                                <li><strong>Poskytovateľ</strong> <br>Universal Technologies s. r. o.</li>
                                <li><strong>Adresa</strong> <br>Trieda SNP 1534/22 <br>Banská Bystrica, SVK</li>
                                <li><strong>IČO</strong> <br>53 058 968</li>
                                <li><strong>Email</strong> <br><a href="">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="outer-container">
                <!--Footer Bottom Shape-->
                <div class="bottom-shape-box"><div class="bg-shape"></div></div>

                <div class="auto-container">
                    <!--Scroll to top-->
                    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>
                    <!--Scroll to top-->
                    <div class="row clearfix">
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="copyright"><span class="logo-icon"></span> &copy; Copyrights 2020 - 2021 <a href="#">HernáZóna.eu</a> - All Rights Reserved</div>
                        </div>
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="social-links">
                                <ul class="default-social-links">
                                    <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </footer>
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/script.js"></script>

</body>
</html>