<!DOCTYPE html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YZ21QB3167"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YZ21QB3167');
</script>
<meta charset="utf-8">
<title>HernáZóna.eu | O nás</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive File -->
<link href="css/responsive.css" rel="stylesheet">



<link rel="shortcut icon" href="images/hzlogodogo2.png" type="image/x-icon">
<link rel="icon" href="images/favicon2.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"><div class="icon"></div></div>

    <!-- Main Header -->
    <header class="main-header header-style-two">
        <div class="header-container">

            <!-- Header Top 
            <div class="header-top">
            	<div class="auto-container">
                    <div class="inner clearfix">
                        <div class="top-left">
                            <div class="top-text">Vitajte na portáli HernáZóna.eu</div>
                        </div>
        
                        <div class="top-right">
                            <ul class="info clearfix">
                                <li><a href="#">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- Header Upper -->
            <div class="header-upper">
            	<div class="auto-container">
                    <div class="inner-container clearfix">
                        <!--Logo-->
                        <div class="logo-box">
                            <div class="logo"><a href="index.php" title=""><img src="images/hzlogodogo.png">hernázóna.eu</a></div>
                        </div>
    
                        <!--Nav Box-->
                        <div class="nav-outer clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span></div>
    
                            <!-- Main Menu -->
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="index.php">Domov</a></li>
                                        <li class="current"><a href="about.php">O nás</a></li>
                                        <li><a href="servers.php">Servery</a></li>
                                        <!-- <li><a href="gallery.php">Galéria</a></li> -->
                                        <li><a href="team.php">Team</a></li>
                                        <li><a href="vip.php">VIP</a></li>
                                        <li><a href="faq.php">Faq</a></li>
                                        <li class="dropdown"><a href="rules.php">Podmienky</a>
                                            <ul>
                                                <li><a href="rules.php">Pravidlá</a></li>
                                                <li><a href="gdpr.php">GDPR</a></li>
                                                <li><a href="cookies.php">Cookies</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.php">Kontakt</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            <ul class="social-links clearfix">
                                <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--End Header Upper-->
        </div><!--End Header Container-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.php" title=""><img src="images/hzlogodogo2.png" alt="" title="">hernázóna.eu</a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu clearfix top5">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav><!-- Main Menu End-->

                    <ul class="social-links clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End Sticky Menu -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/hzlogodogo.png" alt="" title="">hernázóna.eu</a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                <!--Social Links-->
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="https://discord.gg/vnPFP42DUA"><span class="fab fa-discord"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- End Main Header -->


    <!--Page Title-->
    <section class="page-banner" style="background-image:url(images/main-slider/huh.jpg);">
        <div class="top-pattern-layer"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>O nás</h1>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Banner-->

	<!--About Section-->
    <section class="about-section">
        <div class="bottom-pattern-layer-dark"></div>

        <div class="about-content">
            <div class="auto-container">
                <div class="row clearfix">
                    <!--Text Column-->
                    <div class="text-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="sec-title"><h2>Objavte HernúZónu</h2><span class="bottom-curve"></span></div>
                            <div class="text">
                                <p>Sme nový herný portál na Slovenskej a Českej scéne. Vznikli sme na základe chabej až žiadnej hernej aktivity zo strany gaming portálov, inak povedané nie sme si vedomý žiadnej veľkej konkurencie a aj ak áno, všetko je to umierajúca scéna. Preto chceme oživiť gaming u nás ale aj v Českej republike. Špecializujeme sa na herné servery, organizujeme v nich súťaže, eventy a samozrejme je vždy o čo súťažiť. Okrem serverov na tomto portály nájdete aj hernú Windcup Ligu na rôzne hry organizovanú nami.</p>
                                <p>Momentálne vlastníme minecraft servery a cs:go servery. Pokiaľ hľadáš hráčov, zahrať si s nami môžeš aj Rocket League alebo Among Us. Stačí sa pripojiť na náš discord a opýtať sa v chate, či niekto nehľadá hráčov. V budúcnosti chceme rozširovať a zdokonalovať naše herné servery. Tak neváhaj a pripoj sa do našej komunity už dnes!</p>
                            </div>
                            <div class="link-box"><a href="servers.php" class="theme-btn btn-style-one"><span class="btn-title">pozri si naše servery</span></a></div>
                        </div>
                    </div>
                    <!--Image Column-->
                    <div class="image-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-box"><img src="images/resource/featured-image-2.jpg" alt="" title=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fact-counter">
            <div class="auto-container">
                <!--Title-->
                <div class="sec-title centered"><h2>Niečo v číslach</h2><span class="bottom-curve"></span></div>

                <div class="row clearfix">
                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                        <div class="count-box"><span class="count-text" data-speed="3000" data-stop="100">0</span></div>
                        <h4 class="counter-title">hráčov denne</h4>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                        <div class="count-box">0<span class="count-text" data-speed="1500" data-stop="5">0</span></div>
                        <h4 class="counter-title">rokov skúseností</h4>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                        <div class="count-box"><span class="count-text" data-speed="1500" data-stop="5">0</span></div>
                        <h4 class="counter-title">členov teamu</h4>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                        <div class="count-box"><span class="count-text" data-speed="3000" data-stop="99">0</span></div>
                        <h4 class="counter-title">pozitívnych hodnotení</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-image-layer" style="background-image: url(images/background/fact-counter-bg.png);"></div>
        
    </section>

    <!--Team Section-->
    <section class="team-section">
        <div class="top-pattern-layer-dark"></div>
        <div class="bottom-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Title-->
            <div class="sec-title centered"><h2>Náš team</h2><span class="bottom-curve"></span></div>

            <div class="row clearfix">
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="#"><img src="images/team/polkov.jpg" alt="" title=""></a></figure>
                        <div class="lower-box">
                            <h3><a href="#">Polkov</a></h3>
                            <div class="designation">Owner, Developer</div>
                        </div>
                        <div class="social-links">
                            <ul class="default-social-links clearfix">
                                <li><a href="https://www.instagram.com/polkov7/" target="_blank"><span class="fab fa-instagram"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="#"><img src="images/team/vitaly.jpg" alt="" title=""></a></figure>
                        <div class="lower-box">
                            <h3><a href="#">Vitaly</a></h3>
                            <div class="designation">Co-Owner</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="#"><img src="images/team/mrsbrunette.jpg" alt="" title=""></a></figure>
                        <div class="lower-box">
                            <h3><a href="#">Mrs.Brunette</a></h3>
                            <div class="designation">Manager, Partner, Streamer</div>
                        </div>
                        <div class="social-links">
                            <ul class="default-social-links clearfix">
                                <li><a href="https://www.instagram.com/nataly7.2/" target="_blank"><span class="fab fa-instagram"></span></a></li>
                                <li><a href="https://www.twitch.tv/slecnabrunet" target="_blank"><span class="fab fa-twitch"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="800ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="#"><img src="images/team/anet.jpg" alt="" title=""></a></figure>
                        <div class="lower-box">
                            <h3><a href="#">NattyGeece</a></h3>
                            <div class="designation">Community Manager, Streamer</div>
                        </div>
                        <div class="social-links">
                            <ul class="default-social-links clearfix">
                                <li><a href="https://www.instagram.com/nattygeece18/" target="_blank"><span class="fab fa-instagram"></span></a></li>
                                <li><a href="https://www.twitch.tv/nattygeece" target="_blank"><span class="fab fa-twitch"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="link-box" style="margin: auto;"><a href="team.php" class="theme-btn btn-style-one"><span class="btn-title">celý team</span></a></div>
            </div>
            
        </div>
        
    </section>

    <!--Sponsors Section-->
    <section class="sponsors-section">
        <div class="auto-container">
            
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class="image-box"><a href="https://www.crew.sk/"><img src="images/clients/crew.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://www.hostcreators.sk/"><img src="images/clients/hostcreators.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://craftlist.org"><img src="images/clients/craftlist.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://www.hostcreators.sk/"><img src="images/clients/hostcreators.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://www.crew.sk/"><img src="images/clients/crew.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://craftlist.org"><img src="images/clients/craftlist.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://www.crew.sk/"><img src="images/clients/crew.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://www.hostcreators.sk/"><img src="images/clients/hostcreators.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="https://craftlist.org"><img src="images/clients/craftlist.png" alt=""></a></figure></li>
                </ul>
            </div>
            
        </div>
    </section><!--End Sponsors Section-->

    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="top-pattern-layer-dark"></div>

        <div class="auto-container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    
                    <!--Column-->
                    <div class="column col-lg-4 col-md-6 col-sm-12">
                        <div class="footer-widget logo-widget">
                            <div class="widget-title">
                                    <h3>O nás</h3>
                                </div>
                            <div class="text">Sme najlepší herný portál na SK/CZ scéne. Tešiť sa u nás môžte na CS:GO a Rocket League ligy a súťaže, minecraft server s mnoho súťažmi o vip a super vip, dungeons súťaže a mnoho ďalšieho.</div>
							<!--Newsletter-->
							<div class="newsletter-form">
								<form method="post" action="contact.php">
									<div class="form-group clearfix">
										<input type="email" name="email" value="" placeholder="Email adresa" required>
										<button type="submit" class="theme-btn newsletter-btn"><span class="icon flaticon-arrows-6"></span></button>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-5 col-md-6 col-sm-12">
                        <div class="footer-widget links-widget">
                            <div class="widget-content">
                                <div class="row clearfix">
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Preskúmaj</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="about.php">O nás</a></li>
                                            <li><a href="servers.php">Servery</a></li>
                                            <li><a href="team.php">Team</a></li>
                                            <li><a href="vip.php">VIP</a></li>
                                            <li><a href="https://discord.gg/vnPFP42DUA" target="_blank">Discord</a></li>
                                            <li><a href="faq.php">Faq</a></li>
                                            <li><a href="contact.php">Kontakt</a></li>
                                        </ul>
                                    </div>
                                    <div class="column col-lg-6 col-md-6 col-sm-12">
                                        <div class="widget-title">
                                            <h3>Podmienky</h3>
                                        </div>
                                        <ul class="list">
                                            <li><a href="rules.php">Pravidlá</a></li>
                                            <li><a href="gdpr.php">GDPR</a></li>
                                            <li><a href="cookies.php">Cookies</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-lg-3 col-md-6 col-sm-12">
                        <div class="footer-widget info-widget">
                            <ul class="contact-info">
                                <li><strong>Poskytovateľ</strong> <br>Universal Technologies s. r. o.</li>
                                <li><strong>Adresa</strong> <br>Trieda SNP 1534/22 <br>Banská Bystrica, SVK</li>
                                <li><strong>IČO</strong> <br>53 058 968</li>
                                <li><strong>Email</strong> <br><a href="">info@hernazona.eu</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="outer-container">
                <!--Footer Bottom Shape-->
                <div class="bottom-shape-box"><div class="bg-shape"></div></div>

                <div class="auto-container">
                    <!--Scroll to top-->
                    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>
                    <!--Scroll to top-->
                    <div class="row clearfix">
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="copyright"><span class="logo-icon"></span> &copy; Copyrights 2020 - 2021 <a href="#">HernáZóna.eu</a> - All Rights Reserved</div>
                        </div>
                        <!-- Column -->
                        <div class="column col-lg-6 col-md-12 col-sm-12">
                            <div class="social-links">
                                <ul class="default-social-links">
                                    <li><a href="https://discord.gg/vnPFP42DUA" target="_blank"><span class="fab fa-discord"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </footer>
	
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-up-arrow"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/script.js"></script>

</body>
</html>